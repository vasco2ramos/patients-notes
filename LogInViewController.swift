//
//  LogInViewController.swift
//  swiftLogin
//
//  Created by  -  on 27/11/2014.
//  Copyright (c) 2014 iOS-Blog. All rights reserved.
//

import Foundation
import UIKit

import Parse
import Bolts

class logInViewController : UIViewController {
    
    let service = "Locksmith"
    let userAccount = "LocksmithUser"
    let key = "myKey"
    
    @IBOutlet var LogInLabel: UILabel!
    
    @IBOutlet var logInUsername: UITextField!
    
    @IBOutlet var logInPassword: UITextField!
    
    @IBOutlet var logInSavePasswordLabel: UILabel!
    
    @IBOutlet var logInsavePasswordSwitch: UISwitch!
    
    @IBAction func logInActionButton(sender: AnyObject) {
        if logInUsername.text != "" && logInPassword.text != "" {
            // HACK HERE
            let user = try? PFUser.logInWithUsername(logInUsername.text!, password: logInPassword.text!)
            if user != nil {
                // Do stuff after successful login.
                if self.logInsavePasswordSwitch.on {
                    do {
                        try Locksmith.saveData(["some key": "some value"], forUserAccount: "myUserAccount")
                    }
                    catch{
                        print("logInActionButton saving data failed")
                    }
                }
                self.performSegueWithIdentifier("LogInSegue", sender: self)
            } else {
                // The login failed. Check error to see why.
            }
        } else {
            self.LogInLabel.text = "All Fields Required"
        }
    }
}