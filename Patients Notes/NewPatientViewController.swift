//
//  NewPatientViewController.swift
//  Patients Notes
//
//  Created by Fluiddo on 9/29/14.
//  Copyright (c) 2014 Fluiddo. All rights reserved.
//

import UIKit

import Parse
import Bolts


class NewPatientViewController: UIViewController {


    @IBOutlet var nameTF: UITextField!
    @IBOutlet var ageTF: UITextField!
    @IBOutlet var identifierTF: UITextField!
    @IBOutlet var genderSC: UISegmentedControl!
    @IBOutlet var stateSC: UISegmentedControl!
    @IBOutlet var admissonDateTF: UITextField!
    @IBOutlet var roomTF: UITextField!
    @IBOutlet var bedTF: UITextField!
    @IBOutlet var reasonTV: UITextView!
    
    var birthDate: NSDate = NSDate()
    var doa: NSDate = NSDate()
    var dobControl: NSDate! = nil
    var doaControl: NSDate! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    /* checks if all fields are filled and not empty */
    func checkFieldsCompletion(){
        // TODO: Check if fields have any values in it
    }
    
    /*
        Saves a patient to parse backend.
    */
    @IBAction func savePatient(sender: AnyObject) {
        
        print("Save Button Pressed")
        
        let patient:PFObject = PFObject(className: "Patient")
        
        // TODO: Add as relation (perhaps it isnt needed)
        // TODO2: Move this method somewhere else
        //let patientUser: PFRelation = patient.relationForKey("user")
        //patientUser.addObject(PFUser.currentUser()!)
        
        patient["user"] = PFUser.currentUser()
        patient["dob"] = birthDate
        patient["room"] = Int(roomTF.text!)
        patient["bed"] = Int(bedTF.text!)
        patient["gender"] = genderSC.selectedSegmentIndex
        patient["reason"] = reasonTV.text
        patient["state"] = stateSC.selectedSegmentIndex
        patient["name"] = nameTF.text
        patient["doa"] = doa
        
        
        // TODO: Add ID Later
        //    newPatient.id = identifierTF.text.toInt()!
        
        patient.saveEventually()
        print("Patient Saved")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
        Uses the date received from birthdate picker to calculate the age of the patient
    */
    func processBirthDate(data: NSDate){
        birthDate=data
        dobControl = birthDate
        let calendar : NSCalendar = NSCalendar.currentCalendar()
        let now: NSDate = NSDate()
        let ageComponents = calendar.components(.Year,
            fromDate: data,
            toDate: now,
            options: [])
        let age = ageComponents.year
        ageTF.text = String(age)
    }
    
    /*
        Uses the date received from doa picker to calculate the number of days the patient has been admited for
    
    */
    func processDOA(data: NSDate){
        doa=data
        doaControl = doa
        let calendar : NSCalendar = NSCalendar.currentCalendar()
        let now: NSDate = NSDate()
        let doaComponents = calendar.components(NSCalendarUnit.Day,
            fromDate: data,
            toDate: now,
            options: [])
        let los = doaComponents.day
        admissonDateTF.text = String(los)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let viewController = segue.destinationViewController as? BirthDateViewController {
            if dobControl != nil{
                print("DOB Sent \(birthDate)")
                viewController.date = birthDate
            }
            viewController.onDataAvailable = {[weak self]
                (data) in
                if let weakSelf = self {
                    weakSelf.processBirthDate(data)
                }
            }
        }
        if let viewController = segue.destinationViewController as? DOAViewController {
            if doaControl != nil{
                print("DOA Sent \(doa)")
                viewController.date = doa
            }
            viewController.onDataAvailable = {[weak self]
                (data) in
                if let weakSelf = self {
                    weakSelf.processDOA(data)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
