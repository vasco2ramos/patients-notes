//
//  BirthDateViewController.swift
//  Patients Notes
//
//  Created by Vasco Ramos on 11/10/14.
//  Copyright (c) 2014 Fluiddo. All rights reserved.
//

import UIKit

import Parse

class BirthDateViewController: UIViewController {

    @IBOutlet var birthDatePicker: UIDatePicker!

    
    var onDataAvailable : ((data: NSDate) -> ())?
    var date: NSDate! = nil
    
    func sendData(data: NSDate) {
        // Whenever you want to send data back to viewController1, check
        // if the closure is implemented and then call it if it is
        self.onDataAvailable?(data: data)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if date != nil{
            self.birthDatePicker.setDate(date, animated: true)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.onDataAvailable?(data: birthDatePicker.date)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
