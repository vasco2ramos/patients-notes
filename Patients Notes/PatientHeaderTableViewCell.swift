//
//  PatientHeaderTableViewCell.swift
//  Patients Notes
//
//  Created by Fluiddo on 1/27/15.
//  Copyright (c) 2015 Fluiddo. All rights reserved.
//

import UIKit

import Parse

class PatientHeaderTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var sectionNumber: UILabel!
    
    @IBOutlet weak var freeBeds: UILabel!

}
