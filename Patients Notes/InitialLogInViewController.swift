//
//  ViewController.swift
//  swiftLogin
//
//  Created by  -  on 27/11/2014.
//  Copyright (c) 2014 iOS-Blog. All rights reserved.
//

import UIKit

class InitialLogInViewController: UIViewController {
    
    
    let service = "Locksmith"
    let userAccount = "LocksmithUser"
    let key = "myKey"
    
    @IBOutlet var connectionCheckLabel: UILabel!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
            let dictionary = Locksmith.loadDataForUserAccount("myUserAccount")
            if let _ = dictionary {
                // TODO: Check if current user is nil
                print("Yes, You're logged in.")
                self.performSegueWithIdentifier("loggedInSegue", sender: self)
            } else {
                self.performSegueWithIdentifier("logInSegue", sender: self)
            }
            
        
        
        /*if let error = error {
            println("Error: \(error)")
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

