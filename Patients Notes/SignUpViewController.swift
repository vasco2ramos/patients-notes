//
//  SignUpViewController.swift
//  swiftLogin
//
//  Created by  -  on 27/11/2014.
//  Copyright (c) 2014 iOS-Blog. All rights reserved.
//

import Foundation

class SignUpViewController : UIViewController {
    
    let service = "Locksmith"
    let userAccount = "LocksmithUser"
    let key = "myKey"
    
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBAction func signup(sender: AnyObject) {
        
        var usrEntered = usernameTF.text
        
        var pwdEntered = passwordTF.text
        
        var emlEntered = emailTF.text
        
        if usrEntered != "" && pwdEntered != "" && emlEntered != "" {
            
            // If not empty then yay, do something
            var user = PFUser()
            
            user.username = usrEntered
            
            user.password = pwdEntered
            
            user.email = emlEntered
            
            user.signUpInBackgroundWithBlock {
                
                (succeeded: Bool!, error: NSError!) -> Void in
                
                if error == nil {
                    
                    // Hooray! Let them use the app now.
                    println("Success")
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                } else {
                    
                    // Show the errorString somewhere and let the user try again.
                    
                }
                
            }
            
        } else {
            
            
            self.messageLabel.text = "All Fields Required"
            
        }
    
    }
}