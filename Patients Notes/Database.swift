//
//  connection.swift
//  Patients Notes
//
//  Created by Vasco Ramos on 15/06/15.
//  Copyright (c) 2015 Fluiddo. All rights reserved.
//

import Foundation

import Parse
import Bolts

class Database{
    
    static let shared = Database()
    
    // Variable that holds the patients
    var patients: PFQuery = PFQuery(className: "Patient")
    
    // Variable that holds the rooms
    var rooms = [Int]()
    
    // Variable that holds the notes
    //var notes?

    func getAllPatients(){
        // Fetch Patients Belonging to this doctor
        
        // TODO: Select the storage
        // patients.fromLocalDatastore()
        
        patients.whereKey("user", equalTo: PFUser.currentUser()!)
        do {
            try PFObject.pinAllInBackground(patients.findObjects())
        } catch {
            print("Invalid Selection.")
        }
        
        
    }
    
    func getPatientsCount() -> Int{
        patients.fromLocalDatastore()
        return patients.countObjects(nil)
    }
    
    func getPatientsCountByRoom(room: Int) -> Int{
        patients.whereKey("room", equalTo: PFUser.currentUser()!)
        return 1
    }
    
    func getPatient(room: PFObject, bed: Int) -> PFObject?{
        patients.fromLocalDatastore()
        patients.whereKey("room", equalTo: room)
        patients.whereKey("bed", equalTo: bed)
        var result = PFObject()
        do {
            result = try patients.getFirstObject() as PFObject!
        } catch {
            print("Get Patient Error.")
        }
        return result
    }
    
    /* 
        Get all Rooms from Parse Backend
    */
    func getAllRooms(){
        // Fetch Rooms Belonging to this doctor
        let findMyRooms: PFQuery! = PFQuery(className: "Rooms")
        findMyRooms.whereKey("user", equalTo: PFUser.currentUser()!)

        var objects = [PFObject]()
        do {
            objects = try findMyRooms.findObjects()
        }
        catch{
            print("Get All Rooms Error.")
        }
        for object in objects {
            self.rooms.append(object.valueForKey("beds") as! Int)
        }
    }

    
    /*
        Locally get Room Count
    */
    func getRoomCount() -> Int{
        return self.rooms.count
    }
    
    /*  
        Locally Get Beds in Room
    */
    func getRoomBeds(room:Int) -> Int {
        return self.rooms[room]
    }
    
    
}