//
//  PatientsTableViewController.swift
//  Patients Notes v4
//
//  Created by Fluiddo on 9/3/14.
//  Copyright (c) 2014 Fluiddo. All rights reserved.
//

import UIKit

class PatientsTableViewController: UITableViewController {

    var rooms = [Int]()
    
    override func viewDidAppear(animated: Bool) {
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Lets fetch this doctors rooms
        Database.shared.getAllRooms()
        // Lets fetch all the patients
        
        

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of rooms.
        return Database.shared.getRoomCount()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in each room.
        return Database.shared.getRoomBeds(section)
    }

   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("patients", forIndexPath: indexPath) as! PatientTableViewCell

        // Query Current Users Patients
        // Configure the cell...

        return cell
    }
    
    override func tableView(tableView: UITableView,
        viewForHeaderInSection section: Int) -> UIView? {

        let  headerCell = tableView.dequeueReusableCellWithIdentifier("patientRoomHeader") as! PatientHeaderTableViewCell
        headerCell.sectionNumber.text = String(section+1);
        
        
        return headerCell
    }


}
